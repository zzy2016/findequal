package main

import (
	"crypto/sha1"
	"strconv"
	"io/ioutil"
	"fmt"
	"os"
)
var whereTheDoc="/home/zhangzhenyuan/注释/asd"
var f, _ = os.Create(whereTheDoc)
func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	initDat1()
	theS,theN:=getArgs()
	for i,ori,new:=0,theS,"";i<theN;i++{
		ori,new = doHash(ori)
		ifOtherLine(i)
		writeIn("No "+findTheNum(i+1,theN)+":")//No 1:
		writeIn(new)
		ori = new
	}
	f.Close()
}

func doHash(str string) (string, string) {
 	h := sha1.New()
	h.Write([]byte(str))
	bs := h.Sum(nil)
	c := ""
	for _, x := range bs {
		if x < 16 {
			c += "0"
		}
		c += strconv.FormatInt(int64(x), 16)
	}

	return str, c
}
//初始化dat1为空
func initDat1(){
	d1 := []byte("")
	err := ioutil.WriteFile(whereTheDoc, d1, 0644)
	check(err)
}

func getArgs()(string,int){
	var inputS string
	var inputN int
	fmt.Print("The String:")
	fmt.Scanln(&inputS)
	fmt.Print("The Num:")
	fmt.Scanln(&inputN)
	return inputS,inputN
}

func writeIn(s string){
	_, err2 := f.WriteString(s)
	check(err2)
}
func ifOtherLine(i int){
	switch {
	case i==0:
	case i%10!=0:
		writeIn(" ")
	case i%10==0:
		writeIn("\n")
	default:
		panic("Excuse?")
	}
}

func findTheNum(i,theN int)string{
	x,y:=0,0//x,i有几位，y，theN有几位
	for theN>0{
		y++
		theN/=10
	}
	for ii:=i;ii>0;{
		x++
		ii/=10
	}
	z:=""
	for t:=(y-x);t>0;t--{
		z+="0"
	}
	z+=strconv.Itoa(i)
	return z
}